package com.atlassian.bitbucket.internal.archive.web;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Retains URL compatibility with older versions of the archive plugin by forwarding requests to the new REST
 * resource added in 2.1.
 *
 * @deprecated in 2.1 for removal in 3.0. Callers should update their URLs to use the new REST resource instead.
 */
@Deprecated
public class ArchiveServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher(buildRestUrl(request))
                .forward(request, response);
    }

    private static String buildRestUrl(HttpServletRequest request) {
        return "/rest/archive/latest" + StringUtils.appendIfMissing(request.getPathInfo(), "/") + "archive";
    }
}
